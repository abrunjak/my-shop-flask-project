from flask import render_template, request, redirect, url_for, send_from_directory, session, flash
from flask_paginate import Pagination
from assets.db_session import addProduct,allProducts, oneProduct, filterProducts, filterProductsActive, activeProducts, inactiveProducts, deactivateProduct, searchProducts, deleteProduct, activateDeactivateProduct, addUser, allUsers, filterUser, changePassword
from datetime import timedelta
from random import choices
import pandas as pd
import matplotlib.pyplot as plt
import string
import os
import bcrypt

#Creating active/inactive pie plot function
def active_inactive_plot(app, number_of_active_products, number_of_inactive_products):
    df = pd.DataFrame({"products": ["Active", "Inactive"], "quantity":[number_of_active_products ,number_of_inactive_products]})
    def absolute_value(val):
        a = round(val/100.*df["quantity"].sum(), 0)
        return int(a)
    plot = df.plot.pie(y="quantity", labels=df["products"], autopct=absolute_value)
    plot.set_ylabel("")
    plt.savefig(os.path.join(app.static_folder, "statistic/active_inacitve.png"), transparent=True)
    plt.close()

#creating added by days bar plot function
def product_days_added(app, all_products:list):
    data = {"Products added by day":[], "Date":[]}
    for product in all_products:
        if product.date_added.strftime("%d.%m.%Y.") in data["Date"]:
            data["Products added by day"][-1]+=1
        else:
            data["Products added by day"].append(1)
            data["Date"].append(product.date_added.strftime("%d.%m.%Y."))
    df = pd.DataFrame(data)
    plot = df.plot.bar()
    plot.set_xticklabels(df['Date'], rotation=15)
    plt.savefig(os.path.join(app.static_folder, "statistic/added_by_days.png"), transparent=True)
    plt.close()

def setup(app):
    @app.route("/index") #index route, calling function to create plot if necessary
    def index():
        deactivateProduct()
        number_of_products = len(allProducts())
        if number_of_products > 0:
            active_inactive_plot(app, len(activeProducts()), len(inactiveProducts()))
            product_days_added(app, allProducts())
        else:
            if os.path.exists(os.path.join(app.static_folder, "statistic/active_inacitve.png")):
                os.remove(os.path.join(app.static_folder, "statistic/active_inacitve.png"))
            if os.path.exists(os.path.join(app.static_folder, "statistic/added_by_days.png")):
                os.remove(os.path.join(app.static_folder, "statistic/added_by_days.png")) 
        return render_template("index.html", number_of_products=number_of_products)
    
    @app.route("/about") #about route
    def about():
        return render_template("about.html")
    
    @app.route("/catalogue") #catalogue route
    def catalogue():
        search = request.args.get("search", type=str, default="")
        if search == "":
            catalogue_products = activeProducts()
        else:
            catalogue_products = searchProducts(search)
        page = request.args.get("page", type=int, default=1)
        per_page = request.args.get("per_page", type=int, default=5)
        total_rows = len(catalogue_products)
        offset = (page-1) * per_page
        products = catalogue_products[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total_rows)
        return render_template("catalogue.html", products=products, pagination=pagination, page_num=page, search=search)
    
    @app.route("/contact") #contact route
    def contact():
        return render_template("contact.html")
    
    @app.route("/login", methods=["POST", "GET"]) #login route, chacking if username and password are matching
    def login():
        if request.method == "GET":
            return render_template("login.html")
        if request.method == "POST":
            try:
                username = request.form.get("username").strip()
                password = request.form.get("password").strip()
                if len(username) < 1:
                    raise Exception("Type username")
                if len(password) < 1:
                    raise Exception("Type password")  
                user = filterUser(username)
                if user is None:
                    raise Exception("User does not exists") 
                hashed_password = user.password.encode('utf-8')
                if bcrypt.checkpw(password.encode('utf-8'), hashed_password):
                    session['username'] = username
                    session.permanent = True
                    return redirect(url_for("index"))  
                else:
                    raise Exception("Incorrect password")
            except Exception as exc:
                return render_template("login.html", username=request.form.get("username"), password=request.form.get("password"), message=exc)

    @app.route("/profile/<user_username>", methods=["POST", "GET"]) #profile route, if user is logged in redirecting to profile, else to catalogue
    def profile(user_username):
        if session.get("username") != None:
            user = filterUser(username=user_username)
            filter = request.args.get("filter", type=str, default=None)
            if filter == "active":
                filter_products = filterProductsActive(user.id, True)
            elif filter == "inactive":
                filter_products = filterProductsActive(user.id, False)
            else:
                filter_products = filterProducts(user.id)
            page = request.args.get("page", type=int, default=1)
            per_page = request.args.get("per_page", type=int, default=5)
            total_rows = len(filter_products)
            offset = (page-1) * per_page
            products = filter_products[offset: offset + per_page]
            pagination = Pagination(page=page, per_page=per_page, total=total_rows)
            return render_template("profile.html",products=products,pagination=pagination, page_num=page, filter=filter)
        else:
            flash("You need to be logged in to access profile")
            return redirect(url_for("catalogue"))

    @app.route("/logout") #logout route, clearing session
    def logout():
        session.pop("username")
        return redirect(url_for("index") )
    
    @app.route("/register", methods=["POST", "GET"]) #register route, if everything is ok encoding password and creating user
    def register():
        if request.method == "GET":
            return render_template("register.html")
        if request.method == "POST":
            try:
                name = request.form.get("name").strip()
                if len(name) < 1:
                    raise Exception("Enter name")
                username = request.form.get("username").strip()
                if len(username) < 1:
                    raise Exception("Enter username")
                email = request.form.get("email").strip().lower()
                if len(email) < 1:
                    raise Exception("Enter email")
                password = request.form.get("password").strip()
                if len(password) < 1:
                    raise Exception("Enter password")
                retype_password = request.form.get("retype_password").strip()
                if len(retype_password) < 1:
                    raise Exception("Retype password")
                if password != retype_password:
                    raise Exception("Passwords are not matching")
                if len(password) < 5:
                    raise Exception("Password is too short at least 5 characters")
                for user in allUsers():
                    if username.lower() == str(user.username).lower():
                        raise Exception("Username exists")
                    elif email.lower() == str(user.email):
                        raise Exception("Email exists")
                hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
                addUser(name, username, email, hashed_password.decode('utf-8'))
                return redirect(url_for("index"))  
            except Exception as exc:
                return render_template("register.html", name=request.form.get("name"), username=request.form.get("username"), email=request.form.get("email"), password=request.form.get("password"), retype_password=request.form.get("retype_password"), message=exc)
            
    @app.route("/change_password", methods=["POST", "GET"]) #change password route, if user is logged in chacking passwords and changing, password must be at least 5 characters long
    def change_password():
        if session.get("username") != None:
            if request.method == "GET":
                return render_template("change_password.html")
            if request.method == "POST":
                try:
                    password = request.form.get("password").strip()
                    if len(password) < 1:
                        raise Exception("Enter password")
                    new_password = request.form.get("new_password").strip()
                    if len(new_password) < 1:
                        raise Exception("Enter new password")
                    retype_new_password = request.form.get("retype_new_password").strip()
                    if len(retype_new_password) < 1:
                        raise Exception("Retype new password")
                    user = filterUser(session["username"])
                    hashed_old_password = user.password.encode('utf-8')
                    if bcrypt.checkpw(password.encode('utf-8'), hashed_old_password):
                        if new_password == retype_new_password:
                            if len(new_password) >= 5:
                                if bcrypt.checkpw(new_password.encode('utf-8'), hashed_old_password):
                                    raise Exception("New password is same as old one")
                                else:
                                    hashed_password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
                                    changePassword(user.username, hashed_password.decode('utf-8'))
                                    return redirect(url_for("profile", user_username=user.username))
                            else:
                                raise Exception("New password is too short at least 5 characters")                    
                        else:
                            raise Exception("New password and old pasword are not matching")
                    else:
                        raise Exception("You entered wrong password")
                except Exception as exc:
                    return render_template("change_password.html", message=exc, password=request.form.get("password").strip(), new_password=request.form.get("new_password").strip(), retype_new_password=request.form.get("retype_new_password").strip())
        else:
            flash("You need to be logged in if you want to change password")
            return redirect(url_for("catalogue"))

    @app.route("/add_product", methods=["POST", "GET"]) # adding product and picture of it, renaming picture before upload
    def add_product():
        if session.get("username") != None:
            user = filterUser(session["username"])
            if request.method == "GET":
                return render_template("add_product.html", contact=user.email)
            if request.method == "POST":
                try:
                    destination_path=""
                    name = request.form.get("name").strip()
                    if len(name) < 1:
                        raise Exception("Enter name")
                    description = request.form.get("description").strip()
                    if len(description) < 1:
                        raise Exception("Enter description")
                    contact = request.form.get("contact").strip()
                    if len(contact) < 1:
                        raise Exception("Enter contact")
                    price = request.form.get("price", type=float)
                    if price == None or price < 0.01:
                        raise Exception("Price not valid")
                    image = request.files["image"]
                    try:
                        file_extensions =  ["JPG","JPEG","PNG","GIF"]
                        uploaded_file_extension = image.filename.split(".")[1]
                        if(uploaded_file_extension.upper() in file_extensions):
                            filename = ''.join(choices(string.ascii_lowercase+string.digits, k=20))+"."+uploaded_file_extension
                            while os.path.exists(os.path.join(app.config["UPLOAD_FOLDER"], filename)):
                                filename = ''.join(choices(string.ascii_lowercase+string.digits, k=20))+"."+uploaded_file_extension
                            destination_path= os.path.join(app.config["UPLOAD_FOLDER"], filename)
                            image.save(destination_path)
                            addProduct(name, description, contact, price, filename, user.id)
                            return redirect(url_for("catalogue"))       
                    except:
                        raise Exception("Enter valid image")          
                except Exception as exc:
                    return render_template("add_product.html", name=request.form.get("name"), description=request.form.get("description"), contact=request.form.get("contact"), message=exc, price=request.form.get("price", type=float))
        else:
           flash("You need to be logged in to add product")
           return redirect(url_for("catalogue")) 
        
    @app.route("/delete/<Number>", methods=["POST", "GET"]) #deleting product if user is logged in
    def delete(Number):
        if session.get("username") != None:
            deleteProduct(Number)
            return redirect(url_for("profile", user_username=session['username']))
        else:
            flash("You need to be logged in to delete product")
            return redirect(url_for("catalogue"))
    
    @app.route("/activate_deactivate/<Number>", methods=["POST", "GET"]) #activating/deactivating product if user is logged in
    def activate_deactivate(Number):
        if session.get("username") != None:
            activateDeactivateProduct(Number)
            return redirect(url_for("profile", user_username=session['username']))
        else:
            flash("You need to be logged in to activate/deactivate product")
            return redirect(url_for("catalogue"))
        
    @app.route("/details/<Number>") #detailed view of product
    def details(Number):
        product=oneProduct(Number)
        if product.active:
            expiry_date = (product.date_active + timedelta(days=30)).strftime("%d.%m.%Y.")
        else:
            expiry_date = "Product deactivated"
        return render_template("details.html", product=oneProduct(Number), expiry_date=expiry_date)        
    
    @app.route("/media/<path:filename>") #route to return uploaded images
    def media(filename):
        return send_from_directory(app.config["UPLOAD_FOLDER"], filename)

    app.add_url_rule("/", "index", index)
