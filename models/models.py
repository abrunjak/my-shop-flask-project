from sqlalchemy import Column,Integer,String, Float, Date, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from datetime import date

#Database models

Base = declarative_base()

class User(Base):
    __tablename__ = "Users"
    id = Column(Integer,primary_key = True, autoincrement=True)
    name = Column(String, nullable=False)
    username = Column(String, unique=True, nullable=False)
    password = Column(String, nullable=False)
    email = Column(String, unique=True, nullable=False)
    date = Column(Date, default=date.today())

class Product(Base):
    __tablename__ = "Products"
    Number = Column(Integer,primary_key = True, autoincrement=True)
    name = Column(String)
    description = Column(String)
    image = Column(String)
    contact = Column(String)
    price = Column(Float)
    date_added = Column(Date, default=date.today())
    date_active = Column(Date, default=date.today())
    user = Column(Integer, ForeignKey("Users.id"))
    active = Column(Boolean, default=True)
