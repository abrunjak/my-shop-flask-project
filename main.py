from flask import Flask
from controllers.controller import setup

'''
@Author: Antonio Brunjak
@Creation date: 16.03.2023.
@Version: 0.8.9.
'''

app = Flask(__name__, template_folder="views", static_folder="static")
app.config["UPLOAD_FOLDER"] =  "assets/images/"
app.config['SESSION_COOKIE_NAME'] = "my_session"
app.config['PERMANENT_SESSION_LIFETIME'] = 3600 #setting session duration
app.secret_key = "dev"

setup(app)

if __name__ == "__main__":
    app.run(debug=True)