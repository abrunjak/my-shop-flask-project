from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models.models import Product, User, Base
from datetime import timedelta, date
from datetime import date
import os

#Script that controls over database

engine = create_engine("sqlite:///assets/MyShopBase.db", connect_args={'check_same_thread':False})
Session = sessionmaker(bind=engine)    
db_session = Session()

def addUser(name, username, email, password):
    new_user = User(name=name, username=username, email=email, password=password)
    db_session.add(new_user)
    db_session.commit()

def allUsers():
    Base.metadata.create_all(engine)
    user = db_session.query(User).all()
    return user

def filterUser(username):
    Base.metadata.create_all(engine)
    user = db_session.query(User).filter_by(username=username).first()
    return user

def changePassword(username, password):
    user = db_session.query(User).filter_by(username=username).first()
    user.password = password
    db_session.commit()

def addProduct(name, description, contact, price, image, user):
    new_product = Product(name=name, description=description, contact=contact, price=price, image=image, user=user)
    db_session.add(new_product)
    db_session.commit()

def allProducts():
    Base.metadata.create_all(engine)
    product = db_session.query(Product).order_by(Product.date_added.asc()).all()
    return product

def activeProducts():
    Base.metadata.create_all(engine)
    active_products = db_session.query(Product).filter_by(active=True).order_by(Product.date_active.desc(), Product.Number.desc()).all()
    return active_products

def inactiveProducts():
    Base.metadata.create_all(engine)
    active_products = db_session.query(Product).filter_by(active=False).order_by(Product.date_active.desc(), Product.Number.desc()).all()
    return active_products

def filterProducts(user):
    products = db_session.query(Product).filter_by(user=user).order_by(Product.active.desc(), Product.date_active.desc(), Product.Number.desc()).all()
    return products

def filterProductsActive(user, active):
    products = db_session.query(Product).filter_by(user=user, active=active).order_by(Product.date_active.desc(), Product.Number.desc()).all()
    return products

def oneProduct(Number):
    product = db_session.query(Product).filter_by(Number=Number).first()
    return product

def searchProducts(search):
    products = db_session.query(Product).filter((Product.name + ' ' + Product.description).like(f'%{search}%')).all()
    return products
    
def deactivateProduct():
    expiry_date = date.today() - timedelta(days=30)
    products = allProducts()
    for product in products:
        if product.date_active < expiry_date:
            product.active = False
            db_session.commit()

def activateDeactivateProduct(Number):
    product = db_session.query(Product).filter_by(Number=Number).first()
    if product.active:
        product.active = False
    else:
        product.active = True
        product.date_active = date.today()
    db_session.commit()

def deleteProduct(Number):
    product = oneProduct(Number)
    db_session.query(Product).filter_by(Number=Number).delete()
    os.remove(f"assets/images/{product.image}")
    db_session.commit()

